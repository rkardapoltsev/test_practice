<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Name;

class MainController extends Controller
{
    public function submit(Request $req){
        $form = new Name();
        $form->name = $req->input('name');
        $form->surname = $req->input('surname');
        
        $form->save();

        return redirect()->route('home');
    }
}
